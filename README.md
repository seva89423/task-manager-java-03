PROJECT INFO
=====================
TASK MANAGER

DEVELOPER INFO
=====================
**NAME:** Vsevolod Zorin

**E-MAIL:** <seva89423@gmail.com>

SOFTWARE
=====================
- JDK 1.8
- MS WINDOWS 10

PROGRAM RUN
=====================
```
java -jar ./task-manager.jar
```
